Hello 👋 my name is Scott (he/him), I'm currently based in Sydney, Australia 🇦🇺. I love HTML and CSS and am passionate about accessiblity, design systems, and performance. Find out more on my [personal website](https://scottdejonge.com/).

I joined GitLab in August 2022. I'm a Senior Frontend Engineer on the Foundations Design System team working on [Pajamas](https://design.gitlab.com/) and [GitLab UI](https://gitlab.com/gitlab-org/gitlab-ui).

## Work style

- I have a strong action bias and will push forward to solve a problem, often taking different approaches
- I will pivot my thinking on a problem to try and find alternative pathways I haven't considered
- I prefer minimal intervention where ever possible, using browser behaviours, or standards to custom solutions

## Weak spots

- I will often implement first, and document after, which can lead to difficultly zooming-out from an implementation detail to a wider context
- I will avoid tackling a problem until I understand how to resolve it
- I won't prioritise something I don't believe is the correct approach

## Communication style

- I primarily use To-dos to track work (please use @sdejonge to get my attention)
- I like to write verbose documentation and comments to help capture context in the moment
- I'm trying to produce more video recordings of exploration and process to improve async communication of problems and solutions